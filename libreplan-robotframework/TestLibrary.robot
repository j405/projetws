*** Settings ***
Library     RequestsLibrary
Library     SeleniumLibrary
Library     String
Resource    Environment.resource


*** Keywords ***
CRUD Test
    [Tags]    robot:continue-on-failure
    [Arguments]    ${typeDataSet}

    # Set endpoint
    ${endpoint}=    Set Variable    ${typeDataSet}[endpoint]

    # Get XML

    # Create
    FOR    ${dataSet}    IN    @{typeDataSet}[dataset]
        ${xml}=    Create XML    ${typeDataSet}    ${dataSet}
        Create    ${endpoint}    ${False}    ${wsreader}    ${xml}
        Create    ${endpoint}    ${True}    ${wswriter}    ${xml}
    END

    # Get all
    @{codeList}=    Get all    ${endpoint}    ${True}    ${wsreader}    ${typeDataSet}[codeRegex]
    @{codeList}=    Get all    ${endpoint}    ${True}    ${wswriter}    ${typeDataSet}[codeRegex]

    # Get and delete
    FOR    ${code}    IN    @{codeList}
        Get by code    ${endpoint}    ${True}    ${wsreader}    ${code}
        Delete by code    ${endpoint}    ${False}    ${wsreader}    ${code}
        Get by code    ${endpoint}    ${True}    ${wswriter}    ${code}
        Delete by code    ${endpoint}    ${True}    ${wswriter}    ${code}
        Get by code    ${endpoint}    ${False}    ${wswriter}    ${code}
    END

Create
    [Arguments]    ${endpoint}    ${passant}    ${user}    ${xml}
    ${auth}=    Evaluate    ("${user}[login]", "${user}[password]")
    TRY
        ${response}=    POST    ${url}${endpoint}    data=${xml}    auth=${auth}
    EXCEPT    AS    ${ex}
        IF    not ${passant}
            Should Start With    ${ex}    HTTPError: 403
        ELSE
            ${response}=    Set Variable    ${ex}
        END
    END
    IF    ${passant}    Status Should Be    201    ${response}

Get all
    [Arguments]    ${endpoint}    ${passant}    ${user}    ${codeRegex}
    ${auth}=    Evaluate    ("${user}[login]", "${user}[password]")
    ${response}=    GET    ${url}${endpoint}    auth=${auth}
    @{codeList}=    Get Regexp Matches    ${response.text}    ${codeRegex}
    Log    ${response.text}
    IF    ${passant}    Status Should Be    200
    RETURN    @{codeList}

Get by code
    [Arguments]    ${endpoint}    ${passant}    ${user}    ${code}
    ${auth}=    Evaluate    ("${user}[login]", "${user}[password]")
    TRY
        ${response}=    GET    ${url}${endpoint}/${code}    auth=${auth}
    EXCEPT    AS    ${ex}
        IF    not ${passant}
            Should Start With    ${ex}    HTTPError: 404
        ELSE
            ${response}=    Set Variable    ${ex}
        END
    END
    IF    ${passant}    Status Should Be    200    ${response}

Delete by code
    [Arguments]    ${endpoint}    ${passant}    ${user}    ${code}
    ${auth}=    Evaluate    ("${user}[login]", "${user}[password]")
    ${response}=    DELETE    ${url}${endpoint}/${code}    auth=${auth}
    IF    ${passant}    Status Should Be    204

Create XML
    [Arguments]    ${typeDataSet}    ${dataSet}
    IF    "${typeDataSet}[type]" == "projects"
        ${xml}=    Set Variable
        ...    <order-list xmlns="http://rest.ws.libreplan.org"><order calendar-name="Default" dependencies-constraints-have-priority="false" init-date="${dataset}[init-date]" name="${dataset}[name]" code="${dataset}[code]"><labels/><material-assignments/><advance-measurements/><criterion-requirements/><children/></order></order-list>
    ELSE IF    "${typeDataSet}[type]" == "resources"
        ${xml}=    Set Variable
        ...    <resource-list xmlns="http://rest.ws.libreplan.org"><worker nif="${dataset}[nif]" surname="${dataset}[surname]" first-name="${dataset}[first-name]" code="${dataset}[code]"></worker></resource-list>
    END
    RETURN    ${xml}
